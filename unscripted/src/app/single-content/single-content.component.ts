import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-single-content',
  templateUrl: './single-content.component.html',
  styleUrls: ['./single-content.component.css']
})
export class SingleContentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  customOptions: any = {
    navSpeed: 700,
    dots: false,
    navText: ['<img src="../../assets/images/left-arrow.png" width="100">',
      '<img src="../../assets/images/arrow.png" width="100">'],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    },
    nav: true
  }
  
}
