import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VideoComponent } from './video/video.component';
import { ContentComponent } from './content/content.component';
import { CreatorComponent } from './creator/creator.component';
import { SingleContentComponent } from './single-content/single-content.component';
import { CreatorFormComponent } from './creator-form/creator-form.component';
import { CollectionsComponent } from './collections/collections.component';

const routes: Routes = [
  {path: '', component: VideoComponent},
  {path: 'content', component: ContentComponent},
  {path: 'creator', component: CreatorComponent},
  {path: 'single-content', component: SingleContentComponent},
  {path: 'work-with-us', component: CreatorFormComponent},
  {path: 'collections', component: CollectionsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
