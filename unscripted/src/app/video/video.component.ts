import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css'],
})
export class VideoComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(function () {
      (document.querySelector('#after-video') as HTMLElement).style.visibility = 'visible';
    }, 3000);
    setTimeout(function () {
      (document.querySelector('.navbar-links') as HTMLElement).style.visibility = 'visible';
    }, 2000);
  }

  // navbar menu
  openNav() {
    (document.querySelector('#mobile-nav') as HTMLElement).style.width = "100%";
  }
  closeNav() {
    (document.querySelector('#mobile-nav') as HTMLElement).style.width = "0px";
  }

}
